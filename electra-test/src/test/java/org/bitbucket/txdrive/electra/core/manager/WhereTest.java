package org.bitbucket.txdrive.electra.core.manager;

import android.content.ContentValues;

import org.hamcrest.MatcherAssert;
import org.junit.Test;

import bean.Contact;

import static org.bitbucket.txdrive.electra.core.query.Restrictions.eq;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.junit.MatcherAssert.assertThat;

public class WhereTest extends AbstractRobolectricTest{

	@Test
	public void delete() {
		em.create(setupContact("name1"));
		em.create(setupContact("name1"));
		em.create(setupContact("name2"));

		long deleteCount = em.where(Contact.class, eq("name", "name1")).delete();
		assertThat(deleteCount, is(2L));
	}


	@Test
	public void bulkDelete() {
		for (int i = 0; i < 10; i++) {
			Contact contact = setupContact("name " + i);
			em.create(contact);
		}

		long deleteCount = em.where(Contact.class).delete();
		assertThat(deleteCount, is(10L));
	}

	@Test
	public void update() {
		em.create(setupContact("name1"));
		em.create(setupContact("name1"));
		em.create(setupContact("name2"));

		ContentValues cv = new ContentValues();
		cv.put("name", "name3");
		long updateCount = em.where(Contact.class, eq("name", "name1")).update(cv);
		assertThat(updateCount, is(2L));

		assertThat(em.where(Contact.class, eq("name", "name3")).select().size(), is(2));
	}

	@Test
	public void bulkUpdate() {
		for (int i = 0; i < 10; i++) {
			Contact contact = setupContact("name " + i);
			em.create(contact);
		}

		ContentValues cv = new ContentValues();
		cv.put("name", "name3");
		long updateCount = em.where(Contact.class).update(cv);
		assertThat(updateCount, is(10L));

		assertThat(em.where(Contact.class, eq("name", "name3")).select().size(), is(10));
	}

	@Test
	public void func() {
		em.create(new ContactBuilder().withName("name1").withCars(1).build());
		em.create(new ContactBuilder().withName("name2").withCars(2).build());
		em.create(new ContactBuilder().withName("name3").withCars(3).build());
		em.create(new ContactBuilder().withName("name4").withCars(4).build());
		em.create(new ContactBuilder().withName("name5").withCars(5).build());


		int max = (int) em.where(Contact.class).func().max("cars");
		MatcherAssert.assertThat(max, is(5));

		max = (int) em.where(Contact.class, eq("name", "name3")).func().max("cars");
		MatcherAssert.assertThat(max, is(3));
	}


	private Contact setupContact(String name) {
		Contact contact = new Contact();
		contact.setName(name);
		return contact;
	}
}
