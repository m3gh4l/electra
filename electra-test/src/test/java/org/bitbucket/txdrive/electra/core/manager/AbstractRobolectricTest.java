package org.bitbucket.txdrive.electra.core.manager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

import org.bitbucket.txdrive.electra.Electra;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import electra.ED;


@RunWith(RobolectricTestRunner.class)
@Config(sdk = Build.VERSION_CODES.JELLY_BEAN, manifest =  Config.NONE)
abstract public class AbstractRobolectricTest {
	public static final String DB_NAME = "electra";

	protected Helper helper;
	protected EntityManager em;

	@Before
	public void setup()  {
		helper =  new Helper(RuntimeEnvironment.application, DB_NAME, 1);
		Electra.configure(ED.TYPES);
		em = Electra.with(helper);
	}

	protected class Helper extends SQLiteOpenHelper {

		public Helper(Context context, String name, int version) {
			super(context, name, null, version);
		}

		@Override
		public void onCreate(SQLiteDatabase sqLiteDatabase) {
			Electra.with(sqLiteDatabase).createTables();
		}

		@Override
		public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVer, int newVer) {
			Electra.with(sqLiteDatabase).updateTables();
		}
	}
}
