package org.bitbucket.txdrive.electra;


import org.bitbucket.txdrive.electra.core.manager.EntityManager;

import java.util.List;

import bean.AllTypeBean;
import bean.Master;
import bean.TableInfo;

import static org.bitbucket.txdrive.electra.core.query.Restrictions.eq;

public class TestUtils {

	public static List<TableInfo> getTableInfo(EntityManager em, String tableName) {
		return em.sql(TableInfo.class, "pragma table_info('" + tableName + "')", null);
	}

	public static Master getIndex(EntityManager em, String tableName, String indexName) {
		return em.select(Master.class).where(eq("tbl_name", tableName),
				eq("type", "index"), eq("name", indexName)).first();
	}

	public static Master getIndex(EntityManager em, String tableName) {
		return em.select(Master.class).where(eq("tbl_name", tableName),
				eq("type", "index")).first();
	}


	public static Master getMasterForTable(EntityManager em, String tableName) {
		return em.select(Master.class).where(eq("tbl_name", tableName),
				eq("type", "table")).first();
	}

}
