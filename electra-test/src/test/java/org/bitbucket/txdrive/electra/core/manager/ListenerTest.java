package org.bitbucket.txdrive.electra.core.manager;

import android.content.ContentValues;

import org.junit.Test;

import bean.Contact;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;

public class ListenerTest extends AbstractRobolectricTest {

	@Test
	public void listenerTest() {
		em.addEntityListener(Contact.class, mockListener);

		Contact c1 = new ContactBuilder().withName("name1").withSum(10.f).build();
		em.create(c1);
		assertThat(mockListener.getChangeType(), is(ChangeType.CREATE));
		assertThat(mockListener.getCalls(), is(1));
		assertThat(mockListener.getEntity().getName(), is("name1"));
		assertThat(mockListener.getEntity().getId(), greaterThan(0L));
		assertThat(mockListener.getEntity().getId(), is(c1.getId()));

		mockListener.reset();

		c1.setName("updated");
		em.update(c1);

		assertThat(mockListener.getChangeType(), is(ChangeType.UPDATE));
		assertThat(mockListener.getCalls(), is(1));
		assertThat(mockListener.getEntity().getName(), is("updated"));
		assertThat(mockListener.getEntity().getId(), is(c1.getId()));

		mockListener.reset();

		em.delete(c1);

		assertThat(mockListener.getChangeType(), is(ChangeType.DELETE));
		assertThat(mockListener.getCalls(), is(1));
		assertThat(mockListener.getEntity().getId(), is(c1.getId()));

		mockListener.reset();

		em.where(Contact.class).delete();

		assertThat(mockListener.getChangeType(), nullValue());
		assertThat(mockListener.getCalls(), is(0));

		mockListener.reset();

		ContentValues cv = new ContentValues();
		cv.put("name", "updated2");
		em.where(Contact.class).update(cv);

		assertThat(mockListener.getChangeType(), nullValue());
		assertThat(mockListener.getCalls(), is(0));


		Contact c2 = new ContactBuilder().withName("name1").withSum(10.f).build();
		em.create(c1);

		mockListener.reset();

		em.where(Contact.class).update(cv);

		assertThat(mockListener.getChangeType(), is(ChangeType.UPDATE));
		assertThat(mockListener.getCalls(), is(1));
		assertThat(mockListener.getEntity(), nullValue());

		mockListener.reset();

		em.where(Contact.class).delete();

		assertThat(mockListener.getChangeType(), is(ChangeType.DELETE));
		assertThat(mockListener.getCalls(), is(1));
		assertThat(mockListener.getEntity(), nullValue());


		em.removeEntityListener(Contact.class, mockListener);
	}

	private MockListener mockListener = new MockListener();

	private  class MockListener implements EntityListener<Contact> {
		private Contact entity;
		private ChangeType changeType;
		private int calls;

		@Override
		public void onChange(ChangeType changeType, Contact entity) {
			this.changeType = changeType;
			this.entity = entity;
			call();
		}

		@Override
		public void onChange(ChangeType changeType) {
			this.changeType = changeType;
			call();
		}

		public Contact getEntity() {
			return entity;
		}

		public ChangeType getChangeType() {
			return changeType;
		}

		private void call() {
			calls++;
		}

		public int getCalls() {
			return calls;
		}

		public void reset() {
			entity = null;
			changeType = null;
			calls = 0;
		}
	};
}
