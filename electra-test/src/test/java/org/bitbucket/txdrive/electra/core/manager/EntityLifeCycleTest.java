package org.bitbucket.txdrive.electra.core.manager;

import org.junit.Test;

import bean.LCEntity;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class EntityLifeCycleTest extends AbstractRobolectricTest {

	@Test
	public void lifeCycle() {
		LCEntity entity = new LCEntity();
		entity.setName("Name");
		em.create(entity);

		assertThat(entity.isPreCreate(), is(true));
		assertThat(entity.isPostCreate(), is(true));
		assertThat(entity.isPreUpdate(), is(false));
		assertThat(entity.isPostUpdate(), is(false));
		assertThat(entity.isPreDelete(), is(false));
		assertThat(entity.isPostDelete(), is(false));
		assertThat(entity.isPostRead(), is(false));


		entity = em.read(LCEntity.class, entity.getId());
		assertThat(entity.isPostRead(), is(true));

		entity.reset();

		em.update(entity);

		assertThat(entity.isPreCreate(), is(false));
		assertThat(entity.isPostCreate(), is(false));
		assertThat(entity.isPreUpdate(), is(true));
		assertThat(entity.isPostUpdate(), is(true));
		assertThat(entity.isPreDelete(), is(false));
		assertThat(entity.isPostDelete(), is(false));
		assertThat(entity.isPostRead(), is(false));

		entity = em.read(LCEntity.class, entity.getId());
		assertThat(entity.isPostRead(), is(true));

		entity.reset();
		em.delete(entity);

		assertThat(entity.isPreCreate(), is(false));
		assertThat(entity.isPostCreate(), is(false));
		assertThat(entity.isPreUpdate(), is(false));
		assertThat(entity.isPostUpdate(), is(false));
		assertThat(entity.isPreDelete(), is(true));
		assertThat(entity.isPostDelete(), is(true));
		assertThat(entity.isPostRead(), is(false));

	}
}
