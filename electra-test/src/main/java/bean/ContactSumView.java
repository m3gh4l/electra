package bean;

import org.bitbucket.txdrive.electra.annotation.Entity;

@Entity(value = "contact", schema = {})
public class ContactSumView {
	private String name;
	private float sum;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getSum() {
		return sum;
	}

	public void setSum(float sum) {
		this.sum = sum;
	}
}
