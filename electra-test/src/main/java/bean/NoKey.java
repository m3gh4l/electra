package bean;

import org.bitbucket.txdrive.electra.annotation.Entity;

@Entity
public class NoKey {
	private String field;

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}
}
