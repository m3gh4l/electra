package bean;

import org.bitbucket.txdrive.electra.annotation.Entity;
import org.bitbucket.txdrive.electra.annotation.Index;
import org.bitbucket.txdrive.electra.annotation.SuperEntity;

@Entity
@SuperEntity
public class SimpleIndex {

	@Index("MyIndex")
	private String name;

	@Index
	private String name2;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName2() {
		return name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}
}
