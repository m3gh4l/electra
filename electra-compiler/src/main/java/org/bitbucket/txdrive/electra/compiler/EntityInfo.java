/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.compiler;

import java.util.ArrayList;
import java.util.List;

import javax.lang.model.element.TypeElement;

import org.bitbucket.txdrive.electra.annotation.Schema;
import org.bitbucket.txdrive.electra.meta.IndexMeta;

public class EntityInfo {
	private TypeElement elementType;
	private String name;
	private String tableName;
	private List<FieldInfo> fieldInfoLis;

	private Schema[] schema;

	private List<String> preCreateMethodNames = new ArrayList<>();
	private List<String> postCreateMethodNames = new ArrayList<>();
	private List<String> postReadMethodNames = new ArrayList<>();
	private List<String> preUpdateMethodNames = new ArrayList<>();
	private List<String> postUpdateMethodNames = new ArrayList<>();
	private List<String> preDeleteMethodNames = new ArrayList<>();
	private List<String> postDeleteMethodNames = new ArrayList<>();
	private List<IndexMeta> indexes = new ArrayList<>();

	public EntityInfo(TypeElement elementType) {
		this.elementType = elementType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public TypeElement getElementType() {
		return elementType;
	}

	public List<FieldInfo> getFieldInfoLis() {
		return fieldInfoLis;
	}

	public void setFieldInfoLis(List<FieldInfo> fieldInfoLis) {
		this.fieldInfoLis = fieldInfoLis;
	}

	public Schema[] getSchema() {
		return schema;
	}

	public void setSchema(Schema[] schema) {
		this.schema = schema;
	}

	public List<String> getPreCreateMethodNames() {
		return preCreateMethodNames;
	}

	public List<String> getPostCreateMethodNames() {
		return postCreateMethodNames;
	}

	public List<String> getPostReadMethodNames() {
		return postReadMethodNames;
	}

	public List<String> getPreUpdateMethodNames() {
		return preUpdateMethodNames;
	}

	public List<String> getPostUpdateMethodNames() {
		return postUpdateMethodNames;
	}

	public List<String> getPreDeleteMethodNames() {
		return preDeleteMethodNames;
	}

	public List<String> getPostDeleteMethodNames() {
		return postDeleteMethodNames;
	}

	public void addPreCreateName(String name) {
		preCreateMethodNames.add(name);
	}

	public void addPostCreateName(String name) {
		postCreateMethodNames.add(name);
	}

	public void addPostReadName(String name) {
		postReadMethodNames.add(name);
	}

	public void addPreUpdateName(String name) {
		preUpdateMethodNames.add(name);
	}

	public void addPostUpdateName(String name) {
		postUpdateMethodNames.add(name);
	}

	public void addPreDeleteName(String name) {
		preDeleteMethodNames.add(name);
	}

	public void addPostDeleteName(String name) {
		postDeleteMethodNames.add(name);
	}

	public List<IndexMeta> getIndexes() {
		return indexes;
	}

	public void setIndexes(List<IndexMeta> indexes) {
		this.indexes = indexes;
	}
}
