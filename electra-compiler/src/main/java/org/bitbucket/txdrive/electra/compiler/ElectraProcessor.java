/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.compiler;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

import org.bitbucket.txdrive.electra.annotation.Auto;
import org.bitbucket.txdrive.electra.annotation.Column;
import org.bitbucket.txdrive.electra.annotation.Converter;
import org.bitbucket.txdrive.electra.annotation.Entity;
import org.bitbucket.txdrive.electra.annotation.Index;
import org.bitbucket.txdrive.electra.annotation.Key;
import org.bitbucket.txdrive.electra.annotation.NotNull;
import org.bitbucket.txdrive.electra.annotation.ReadOnly;
import org.bitbucket.txdrive.electra.annotation.SuperEntity;
import org.bitbucket.txdrive.electra.annotation.Unique;
import org.bitbucket.txdrive.electra.annotation.filter.PostCreate;
import org.bitbucket.txdrive.electra.annotation.filter.PostDelete;
import org.bitbucket.txdrive.electra.annotation.filter.PostRead;
import org.bitbucket.txdrive.electra.annotation.filter.PostUpdate;
import org.bitbucket.txdrive.electra.annotation.filter.PreCreate;
import org.bitbucket.txdrive.electra.annotation.filter.PreDelete;
import org.bitbucket.txdrive.electra.annotation.filter.PreUpdate;

import org.bitbucket.txdrive.electra.compiler.gen.ElectraMetaDataGenerator;
import org.bitbucket.txdrive.electra.compiler.gen.MetaTypeGenerator;
import org.bitbucket.txdrive.electra.meta.IndexMeta;

@SupportedAnnotationTypes({"org.bitbucket.txdrive.electra.annotation.*"})
public class ElectraProcessor extends AbstractProcessor {


	@Override
	public SourceVersion getSupportedSourceVersion() {
		return SourceVersion.latestSupported();
	}

	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		Messager messager = processingEnv.getMessager();
		Types types = processingEnv.getTypeUtils();
		Elements elements = processingEnv.getElementUtils();

		List<EntityInfo> entityInfoList = new ArrayList<>();

		for (Element element : roundEnv.getElementsAnnotatedWith(Entity.class)) {
			TypeElement typeElement = ProcessorUtils.getTypeElementOf(element);

			if (typeElement == null) {
				continue;
			}


			EntityInfo entityInfo = new EntityInfo(typeElement);
			Entity entity = typeElement.getAnnotation(Entity.class);
			entityInfo.setName(typeElement.getSimpleName().toString());
			entityInfo.setTableName(entity.value());
			entityInfo.setSchema(entity.schema());

			processIndexes(entityInfo, entity);

			processFIlters(typeElement, entityInfo);

			List<FieldInfo> fieldInfoList = new ArrayList<>();
			List<VariableElement> variables = getVariableElements(elements, typeElement);

			processFields(fieldInfoList, variables);
			entityInfo.setFieldInfoLis(fieldInfoList);
			entityInfoList.add(entityInfo);
		}


		for (EntityInfo entityInfo : entityInfoList) {
			MetaTypeGenerator metaTypeGenerator = new MetaTypeGenerator(processingEnv, entityInfo);
			metaTypeGenerator.generate();
		}

		if (entityInfoList.size() > 0) {
			ElectraMetaDataGenerator electraMetaDataGenerator =
					new ElectraMetaDataGenerator(processingEnv, entityInfoList, "ED");
			electraMetaDataGenerator.generate();
		}

		return false;
	}

	private void processFIlters(TypeElement typeElement, EntityInfo entityInfo) {
		List<ExecutableElement> methods = ProcessorUtils.getMethods(typeElement);

		for (ExecutableElement ee : methods) {
			String methodName = ee.getSimpleName().toString();

			if (ee.getAnnotation(PreCreate.class) != null) {
				entityInfo.addPreCreateName(methodName);
			}

			if (ee.getAnnotation(PostCreate.class) != null) {
				entityInfo.addPostCreateName(methodName);
			}

			if (ee.getAnnotation(PostRead.class) != null) {
				entityInfo.addPostReadName(methodName);
			}

			if (ee.getAnnotation(PreUpdate.class) != null) {
				entityInfo.addPreUpdateName(methodName);
			}

			if (ee.getAnnotation(PostUpdate.class) != null) {
				entityInfo.addPostUpdateName(methodName);
			}

			if (ee.getAnnotation(PreDelete.class) != null) {
				entityInfo.addPreDeleteName(methodName);
			}

			if (ee.getAnnotation(PostDelete.class) != null) {
				entityInfo.addPostDeleteName(methodName);
			}
		}
	}

	private List<VariableElement> getVariableElements(Elements elements, TypeElement typeElement) {
		List<VariableElement> result = new ArrayList<>();
		List<String> names = new ArrayList<>();

		List<VariableElement> variableElements = ProcessorUtils.getFilteredFields(typeElement);
		result.addAll(filterVariableElements(variableElements, names));

		TypeMirror superclass = typeElement.getSuperclass();

		while (superclass != null) {
			TypeElement superTypeElement = elements.getTypeElement(superclass.toString());

			if (superTypeElement != null) {
				if (superTypeElement.getAnnotation(SuperEntity.class) == null) {
					break;
				}

				List<VariableElement> superTypeVariables = ProcessorUtils.getFilteredFields(superTypeElement);
				result.addAll(filterVariableElements(superTypeVariables, names));

				superclass = superTypeElement.getSuperclass();
				if (superclass.getKind() == TypeKind.NONE) {
					break;
				}
			}
		}

		return result;
	}

	private List<VariableElement> filterVariableElements(List<VariableElement> variableElements,
	                                                     List<String> names) {
		List<VariableElement> result = new ArrayList<>();

		for (VariableElement ve : variableElements) {
			String name = ve.getSimpleName().toString();
			if (!names.contains(name)) {
				result.add(ve);
				names.add(name);
			}
		}

		return result;
	}


	private void processFields(List<FieldInfo> fieldInfoList,
	                           List<VariableElement> variableElements) {

		for (VariableElement ve : variableElements) {
			processField(fieldInfoList, ve);
		}
	}

	private void processField(List<FieldInfo> fieldInfoList, VariableElement ve) {
		FieldInfo fieldInfo = new FieldInfo(ve);
		processField(fieldInfo, ve);
		fieldInfoList.add(fieldInfo);
	}

	private void processField(FieldInfo fieldInfo, VariableElement ve) {
		String name = ve.getSimpleName().toString();
		fieldInfo.setName(name);
		fieldInfo.setKey(ve.getAnnotation(Key.class) != null);
		fieldInfo.setAuto(ve.getAnnotation(Auto.class) != null);
		fieldInfo.setUnique(ve.getAnnotation(Unique.class) != null);
		fieldInfo.setNotNull(ve.getAnnotation(NotNull.class) != null);
		fieldInfo.setReadOnly(ve.getAnnotation(ReadOnly.class) != null);

		fieldInfo.setSetter(ProcessorUtils.setterName(name));
		fieldInfo.setGetter(ProcessorUtils.getterName(name, ve.asType()));

		fieldInfo.setConverter(getConverter(ve));

		Column column = ve.getAnnotation(Column.class);
		if (column != null) {
			fieldInfo.setColumnName(column.value());
		}

		Index index = ve.getAnnotation(Index.class);
		if (index != null) {
			fieldInfo.setIndex(new IndexMeta(index.value(), null));
		}

	}

	private String getConverter(VariableElement ve) {
		String result = null;

		AnnotationMirror annotationMirror = ProcessorUtils.findAnnotationMirror(ve, Converter.class);

		if (annotationMirror != null) {
			AnnotationValue value = ProcessorUtils.findAnnotationValue(annotationMirror, "value");
			result = value != null ? value.getValue().toString() : null;
		}

		if (result == null) {
			result = ProcessorUtils.getConverter(ve);
		}

		return result;
	}

	private void processIndexes(EntityInfo entityInfo, Entity entity) {
		Index[] indexes = entity.indexes();
		List<IndexMeta> indexMetaList = new ArrayList<>();

		if (indexes != null && indexes.length > 0) {
			for (Index index : indexes) {
				indexMetaList.add(new IndexMeta(index.value(), index.columnNames()));
			}
		}
		entityInfo.setIndexes(indexMetaList);
	}
}
