/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.compiler;

import java.lang.annotation.Annotation;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.SimpleTypeVisitor6;
import javax.lang.model.util.Types;

import org.bitbucket.txdrive.electra.annotation.Ignore;

import static org.bitbucket.txdrive.electra.core.converter.FieldConverters.*;

public class ProcessorUtils {
	private static HashMap<String, String> fieldConverters = new HashMap<>();

	static {
		addConverter(byte.class, ByteConverter.class);
		addConverter(Byte.class,ByteConverter.class);
		addConverter(short.class, ShortConverter.class);
		addConverter(Short.class, ShortConverter.class);
		addConverter(int.class,IntegerConverter.class);
		addConverter(Integer.class, IntegerConverter.class);
		addConverter(long.class,LongConverter.class);
		addConverter(Long.class, LongConverter.class);
		addConverter(float.class, FloatConverter.class);
		addConverter(Float.class, FloatConverter.class);
		addConverter(double.class, DoubleConverter.class);
		addConverter(Double.class, DoubleConverter.class);
		addConverter(boolean.class,BooleanConverter.class);
		addConverter(Boolean.class, BooleanConverter.class);

		addConverter(String.class, StringConverter.class);
		addConverter(Date.class, DateConverter.class);
		addConverter(byte[].class,BlobArrayConverter.class);
		addConverter(BigDecimal.class, BigDecimalConverter.class);
		addConverter(BigInteger.class, BigIntegerConverter.class);

	}

	private static void addConverter(Class type, Class converterClass) {
		fieldConverters.put(type.getCanonicalName(), converterClass.getCanonicalName());
	}

	public static List<ExecutableElement> getMethods(Element element) {
		return ElementFilter.methodsIn(element.getEnclosedElements());
	}

	public static List<ExecutableElement> getConstructors(Element element) {
		return ElementFilter.constructorsIn(element.getEnclosedElements());
	}

	public static List<VariableElement> getFilteredFields(Element element) {
		return filterFields(getFields(element));
	}

	public static List<VariableElement> getFields(Element element) {
		return ElementFilter.fieldsIn(element.getEnclosedElements());
	}

	public static List<VariableElement> filterFields(List<VariableElement> variableElements) {
		List<VariableElement> result = new ArrayList<>();

		for(VariableElement variableElement : variableElements) {
			if (!isIgnoreField(variableElement)) {
				result.add(variableElement);
			}
		}

		return result;
	}

	public static TypeElement getTypeElementOf(Element element) {
		return element.getKind().equals(ElementKind.CLASS) ? (TypeElement) element : null;
	}

	public static boolean isIgnoreField(Set<Modifier> modifiers) {
		boolean result = false;

		for (Modifier modifier : modifiers) {
			if (Modifier.TRANSIENT.equals(modifier)
					|| Modifier.FINAL.equals(modifier)
					|| Modifier.STATIC.equals(modifier)) {

				result = true;
				break;
			}
		}

		return result;
	}

	public static boolean isIgnoreField(VariableElement ve) {
		boolean isIgnored = isIgnoreField(ve.getModifiers());

		if (!isIgnored) {
			isIgnored = ve.getAnnotation(Ignore.class) != null;
		}

		return isIgnored;
	}

	public static boolean isTypeOf(TypeElement element, Class type) {
		String canonicalName = type.getCanonicalName();
		return equalToQualifiedName(element, canonicalName);
	}

	public static boolean equalToQualifiedName(TypeElement element, String name) {
		return element.getQualifiedName().toString().equals(name);
	}

	public static String getStringValue(String value, String defaultValue) {
		return value == null || "".equals(value.trim()) ? defaultValue : value;
	}

	public static String stringFirstUpperCase(String str) {
		return str.substring(0, 1).toUpperCase() + str.substring(1);
	}

	public static String getterName(String name, TypeMirror typeMirror) {
		return typeMirror.getKind().equals(TypeKind.BOOLEAN) ? "is" + stringFirstUpperCase(name) :
				"get" + stringFirstUpperCase(name);
	}

	public static String setterName(String name) {
		return "set" + stringFirstUpperCase(name);
	}

	public static List<TypeMirror> listGenericTypeArguments(TypeMirror typeMirror) {
		final List<TypeMirror> typeArguments = new ArrayList<>();
		typeMirror.accept(new SimpleTypeVisitor6<Void, Void>() {
			@Override
			public Void visitDeclared(DeclaredType declaredType, Void v) {
				if (!declaredType.getTypeArguments().isEmpty()) {
					typeArguments.addAll(declaredType.getTypeArguments());
				}
				return null;
			}
			@Override
			protected Void defaultAction(TypeMirror typeMirror, Void v) {
				return null;
			}
		}, null);
		return typeArguments;
	}

	public static boolean isOverrideMethod(Types typesUtils, TypeElement element, String methodName) {
		while (element != null) {
			List<ExecutableElement> executableElements = ElementFilter.methodsIn(element.getEnclosedElements());

			for (ExecutableElement method : executableElements) {
				boolean hasMethod = method.getSimpleName().toString().equals(methodName);

				if (hasMethod) {
					return true;
				}
			}

			TypeMirror typeMirror = element.getSuperclass();

			if (typeMirror.getKind() == TypeKind.NONE) {
				break;
			} else {
				element = (TypeElement) typesUtils.asElement(typeMirror);
			}
		}

		return false;
	}

	public static AnnotationMirror findAnnotationMirror(Element element, Class<? extends Annotation> annotation) {
		return findAnnotationMirror(element, annotation.getName());
	}

	public static AnnotationMirror findAnnotationMirror(Element element, String qualifiedName) {
		List<? extends AnnotationMirror> annotationMirrors = element.getAnnotationMirrors();
		for (AnnotationMirror annotationMirror : annotationMirrors) {
			TypeElement typeElement = (TypeElement) annotationMirror.getAnnotationType().asElement();

			if (typeElement.getQualifiedName().toString().equals(qualifiedName)) {
				return annotationMirror;
			}
		}

		return null;
	}

	public static AnnotationValue findAnnotationValue(AnnotationMirror mirror, String name) {
		Map<? extends ExecutableElement, ? extends AnnotationValue> elementValues = mirror.getElementValues();
		for (ExecutableElement key : elementValues.keySet()) {
			if (key.getSimpleName().toString().equals(name)) {
				return elementValues.get(key);
			}
		}

		return null;
	}

	public static String getConverter(VariableElement variableElement) {
		TypeMirror typeMirror = variableElement.asType();
		return fieldConverters.get(typeMirror.toString());
	}

	public static boolean isEmpty(String str) {
		return str == null || str.trim().equals("");
	}


	public static boolean hasModifier(Set<Modifier> actual, Modifier... expected) {
		boolean result = true;

		for (Modifier modifier : expected) {
			if (!actual.contains(modifier)) {
				result = false;
				break;
			}
		}

		return result;
	}

	public static ExecutableElement findMethodByName(List<ExecutableElement> methods, String name) {
		ExecutableElement result = null;

		for (ExecutableElement ee : methods) {
			if (ee.getSimpleName().toString().equals(name)) {
				result = ee;
				break;
			}
		}

		return result;
	}


}
