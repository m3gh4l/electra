/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.core.query;

import android.content.ContentValues;

import java.util.List;

import org.bitbucket.txdrive.electra.core.manager.ChangeType;
import org.bitbucket.txdrive.electra.core.manager.ElectraDatabase;

import org.bitbucket.txdrive.electra.core.manager.EntityManager;
import org.bitbucket.txdrive.electra.meta.MetaField;
import org.bitbucket.txdrive.electra.meta.MetaType;
import org.bitbucket.txdrive.electra.meta.MetaTypes;
import rx.Observable;
import rx.functions.Func0;

public class Where<T> {
	private Expression where;
	private EntityManager em;
	private Class<T> type;
	private MetaType<T> metaType;
	private QueryContext queryContext;

	public Where(EntityManager em, Class<T> type, Expression... where) {
		if (where != null && where.length > 0) {
			this.where = Restrictions.and(where);
		}

		this.type = type;
		this.em = em;
		this.metaType = MetaTypes.typeOf(type);
		this.queryContext = new QueryContext(metaType);
	}

	public long delete() {
		ElectraDatabase db = em.getDatabase();

		resolveWhere();

		long count = db.delete(metaType.getTableName(), getWhereSql());

		if (count > 0) {
			metaType.notifyChange(ChangeType.DELETE);
		}

		return count;
	}

	public long update(ContentValues cv) {
		return update(cv, true);
	}

	public long update(ContentValues cv, boolean resolve) {
		ElectraDatabase db = em.getDatabase();

		resolveWhere();

		ContentValues updateCV = resolve ? resolveContentValues(metaType, cv) : cv;
		long count = db.update(metaType.getTableName(), updateCV, getWhereSql());

		if (count > 0) {
			metaType.notifyChange(ChangeType.UPDATE);
		}

		return count;
	}

	private ContentValues resolveContentValues(MetaType<?> metaType, ContentValues cv) {
		QueryContext qc = new QueryContext(metaType);

		for (String name : cv.keySet()) {
			qc.validateName(name);
		}

		ContentValues resolvedCV = new ContentValues();
		for (MetaField metaField : metaType.getMetaFields()) {
			String name = metaField.getName();
			if (cv.containsKey(name)) {
				Object value = cv.get(name);
				metaField.getConverter().populateContentValues(metaField.getColumnName(),
						value, resolvedCV);
			}
		}

		return resolvedCV;
	}

	public List<T> select() {
		return em.select(type).where(where).list();
	}

	public Func<T> func() {
		return new Func<>(type, em, where);
	}

	private void resolveWhere() {
		if (where != null) {
			where.resolveNames(queryContext);
		}

	}

	private String getWhereSql() {
		return where != null ? where.toSql() : null;
	}

	public Observable<Long> deleteObservable() {
		return Observable.defer(new Func0<Observable<Long>>() {
			@Override
			public Observable<Long> call() {
				return Observable.just(delete());
			}
		});
	}

	public Observable<Long> updateObservable(final ContentValues contentValues) {
		return updateObservable(contentValues, true);
	}

	public Observable<Long> updateObservable(final ContentValues contentValues, final boolean resolve) {
		return Observable.defer(new Func0<Observable<Long>>() {
			@Override
			public Observable<Long> call() {
				return Observable.just(update(contentValues, resolve));
			}
		});
	}

	public Observable<List<T>> selectObservable() {

		return Observable.defer(new Func0<Observable<List<T>>>() {
			@Override
			public Observable<List<T>> call() {
				return Observable.just(select());
			}
		});
	}

}
