/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.core.manager;

import java.util.Collection;
import java.util.List;

import org.bitbucket.txdrive.electra.annotation.Entity;
import org.bitbucket.txdrive.electra.core.query.EntityCursor;
import org.bitbucket.txdrive.electra.core.query.Expression;
import org.bitbucket.txdrive.electra.core.query.Func;
import org.bitbucket.txdrive.electra.annotation.Schema;
import org.bitbucket.txdrive.electra.core.query.Property;
import org.bitbucket.txdrive.electra.core.query.Select;
import org.bitbucket.txdrive.electra.core.query.Where;
import rx.Observable;

public interface EntityManager {
	/**
	 * Close the database connection
	 * @since 1.0.0
	 *
	 */
	void close();

	/**
	 * Check if the database is open
	 * @since 1.0.0
	 * @return true if database is open
	 */
	boolean isOpen();

	/**
	 * Create tables and indexes.
	 * Create table and indexes only if the entity ({@link Entity})
	 * schema contains {@link Schema#CREATE}.<br>
	 * @since 1.0.0
	 * @see Entity
	 * @see Schema
	 */
	void createTables();

	/**
	 * Create table and indexes by class type.
	 * Create table and indexes only if the entity  ({@link Entity})
	 * schema contains {@link Schema#CREATE}.<br>
	 * @param type class of entity.
	 * @param <T> Type of entity.
	 * @since 1.0.0
	 * @see Entity
	 * @see Schema
	 */
	<T> void createTable(Class<T> type);

	/**
	 * Update and/or create tables(if it does not exists) and create/recreate indexes.
	 * Update table only if the entity ({@link Entity})
	 * schema contains {@link Schema#UPDATE}.<br>
	 * For update - support only add db column.
	 * @since 1.0.0
	 * @see Entity
	 * @see Schema
	 */
	void updateTables();

	/**
	 * Update or create(if it does not exists) table and create/recreate indexes by entity type.
	 * Update table only if the entity ({@link Entity})
	 * schema contains {@link Schema#UPDATE}.<br>
	 * For update - support only add db column.
	 * @since 1.0.0
	 * @see Entity
	 * @see Schema
	 * @param <T> type of entity
	 * @param type class of entity
	 */
	<T> void updateTable(Class<T> type);

	/**
	 * Drop tables.
	 * Drop table only if the entity ({@link Entity})
	 * schema contains {@link Schema#DROP}.<br>
	 * @since 1.0.0
	 * @see Entity
	 * @see Schema
	 */
	void dropTables();

	/**
	 * Drop tables.
	 * Drop table only if the entity ({@link Entity})
	 * schema contains {@link Schema#DROP}.<br>
	 * @since 1.0.0
	 * @see Entity
	 * @see Schema
	 * @param <T> type of entity
	 * @param type class of entity
	 */
	<T> void dropTable(Class<T> type);

	/**
	 * Return DatabaseWrapper
	 * @return instance of wrapped database
	 */
	ElectraDatabase getDatabase();

	/**
	 * Register entity listener by type
	 * @since 1.0.0
	 * @param type entity type
	 * @param entityListener entity listener
	 * @param <T> entity type
	 */
	<T> void addEntityListener(Class<T> type, EntityListener<T> entityListener);

	/**
	 * Unregister entity listener by type
	 * @since 1.0.0
	 * @param type entity class
	 * @param entityListener entity listener
	 * @param <T> entity type
	 */
	<T> void removeEntityListener(Class<T> type, EntityListener<T> entityListener);

	/**
	 * Check if the entity exists in the database by primary keys
	 * @since 1.0.0
	 * @param type entity class
	 * @param key entity primary keys in order how they are defined in the entity class
	 * @param <T> entity type
	 * @return true if entity exists
	 */
	<T> boolean exists(Class<T> type, Object... key);

	/**
	 * Check if the entity exists in the database by expression
	 * @since 1.0.0
	 * @param type entity class
	 * @param where expression
	 * @param <T> entity type
	 * @return true if entity exists
	 * @see Expression
	 */
	<T> boolean exists(Class<T> type, Expression... where);

	/**
	 * Save an entity. If the entity does not exists, it will create a new one,
	 * if the entity exists, it will update the entity.<BR>
	 * When create the entity: if the entity has one @Key long field,
	 * row id will be set into this field.
	 * @since 1.0.0
	 * @param entity entity instance
	 * @param <T> entity type
	 * @return true if the entity is saved
	 */
	<T> boolean save(T entity);

	/**
	 * Save entities in one transaction
	 * @since 1.0.0
	 * @param entities collection of entities
	 * @return true if the transaction completed successfully
	 * @see #save
	 */
	boolean save(Collection<?> entities);

	/**
	 * Create an entity. Always create a new entity. if the entity has one @Key long field,
	 * row id will be set into this field.
	 * @since 1.0.0
	 * @param entity entity instance
	 * @param <T> entity type
	 * @return rowId
	 */
	<T> long create(T entity);

	/**
	 * Create entities in one transaction
	 * @since 1.0.0
	 * @param entities collection of entities
	 * @return true if transaction completed successfully
	 * @see #create
	 */
	boolean create(Collection<?> entities);

	/**
	 * Read the entity by primary keys
	 * @since 1.0.0
	 * @param type entity class
	 * @param key entity primary keys in order how they are defined in the entity class
	 * @param <T> entity type
	 * @return returns entity object, if the entity does not exist returns null
	 */
	<T> T read(Class<T> type, Object... key);

	/**
	 * Update an entity
	 * @since 1.0.0
	 * @param entity entity instance
	 * @param <T> entity type
	 * @return true if entity is updated
	 */
	<T> boolean update(T entity);

	/**
	 * Update entities in one transaction
	 * @since 1.0.0
	 * @param entities collection of entities
	 * @return if the transaction completed successfully
	 * @see #update
	 */
	boolean update(Collection<?> entities);

	/**
	 * Delete an entity
	 * @since 1.0.0
	 * @param entity entity instance
	 * @param <T> entity type
	 * @return true if entity is deleted
	 */
	<T> boolean delete(T entity);

	/**
	 * Delete entities in one transaction
	 * @since 1.0.0
	 * @param entities collection of entities
	 * @return true if the transaction completed successfully
	 * @see #delete
	 */
	boolean delete(Collection<?> entities);

	/**
	 * Create Select query builder
	 * @since 1.0.0
	 * @param type entity class
	 * @param property field names which will be used(processed) in the selection.
	 * Without properties - will be processed all fields(*).
	 * @param <T> entity type
	 * @return Select query builder
	 * @see Property
	 */
	<T> Select<T> select(Class<T> type, Property... property);

	/**
	 * Create Where query builder. It is used for batch operation and simplify access to functions.
	 * @since 1.0.0
	 * @param type entity class
	 * @param where Expression, without expression - without "where" (apply for all records).
	 * @param <T> entity type
	 * @return Where query builder
	 * @see Expression
	 */
	<T> Where<T> where(Class<T> type, Expression... where);

	/**
	 * Create function query builder
	 * @since 1.0.0
	 * @param type entity class
	 * @param <T> entity type
	 * @return Function query builder
	 * @see Expression
	 */
	<T> Func<T> func(Class<T> type);

	/**
	 * Sql Raw query
	 * @since 1.0.0
	 * @param type entity class
	 * @param property field names which will be populated into the entity.
	 * @param sql raw sql query. Names like :name: can be used
	 * @param args selection args
	 * Without properties - will be used all fields(*).
	 * @param <T> entity type
	 * @return Entity Cursor
	 * @see EntityCursor
	 * */
	<T> EntityCursor<T> sqlEntityCursor(Class<T> type, String sql, String[] args, Property... property);

	/**
	 * Sql Raw query
	 * @since 1.0.0
	 * @param type entity class
	 * @param property field names which will be populated into the entity.
	 * @param sql raw sql query. Names like :name: can be used
	 * @param args selection args
	 * Without properties - will be used all fields(*).
	 * @param <T> entity type
	 * @return list of entities
	 * @see Property
	 */
	<T> List<T> sql(Class<T> type, String sql, String[] args, Property... property);


	/**
	 * Return first value of raw sql query
	 * @since 1.0.0
	 * @param type entity class
	 * @param property field names which will be populated into the entity.
	 * @param sql raw sql query. Names like :name: can be used
	 * @param args selection args
	 * Without properties - will be used all fields(*).
	 * @param <T> entity type
	 * @return first result , null if not found
	 * @see Property
	 */
	<T> T sqlFirstResult(Class<T> type, String sql, String[] args, Property... property);

	/**
	 * RxJava wrapper of {@link #close()}
	 * @return Observable
	 * @since 1.0.0
	 */
	Observable<Void> closeObservable();

	/**
	 * RxJava wrapper of {@link #isOpen()} )}
	 * @return Observable
	 * @since 1.0.0
	 */
	Observable<Boolean> isOpenObservable();

	/**
	 * RxJava wrapper of {@link #exists(Class, Object...)}
	 * @param <T> entity type
	 * @param type entity class
	 * @param key Entity primary keys in order how they are defined in the entity class
	 * @return Observable
	 * @since 1.0.0
	 */
	<T> Observable<Boolean> existsObservable(Class<T> type, Object... key);

	/**
	 * RxJava wrapper of {@link #exists(Class, Expression...)}
	 * @param <T> entity type
	 * @param type entity class
	 * @param where Expression
	 * @return Observable
	 * @since 1.0.0
	 */
	<T> Observable<Boolean> existsObservable(Class<T> type, Expression... where);

	/**
	 * RxJava wrapper of {@link #save(Object)}
	 * @param entity entity instance
	 * @param <T> entity type
	 * @return Observable
	 * @since 1.0.0
	 */
	<T> Observable<Boolean> saveObservable(T entity);

	/**
	 * RxJava wrapper of {@link #save(Collection)}
	 * @param entities collection of entities
	 * @return Observable
	 * @since 1.0.0
	 */
	Observable<Boolean> saveObservable(Collection<?> entities);

	/**
	 * RxJava wrapper of {@link #create(Object)}
	 * @param <T> entity type
	 * @param entity entity instance
	 * @return Observable
	 * @since 1.0.0
	 */
	<T> Observable<Long> createObservable(T entity);

	/**
	 * RxJava wrapper of {@link #create(Collection)}
	 * @param entities collection of entities
	 * @return Observable
	 * @since 1.0.0
	 */
	Observable<Boolean> createObservable(Collection<?> entities);

	/**
	 * RxJava wrapper of {@link #read(Class, Object...)}
	 * @param type entity class
	 * @param key entity primary keys in order how they are defined in the entity class
	 * @param <T> entity type
	 * @return Observable
	 * @since 1.0.0
	 */
	<T> Observable<T> readObservable(Class<T> type, Object... key);

	/**
	 * RxJava wrapper of {@link #update(Object)}
	 * @param <T> entity type
	 * @param entity entity instance
	 * @return Observable
	 * @since 1.0.0
	 */
	<T> Observable<Boolean> updateObservable(T entity);

	/**
	 * RxJava wrapper of {@link #update(Collection)}
	 * @param entities collection of entities
	 * @return Observable
	 * @since 1.0.0
	 */
	Observable<Boolean> updateObservable(Collection<?> entities);

	/**
	 * RxJava wrapper of {@link #delete(Object)}
	 * @param entity entity instance
	 * @param <T> entity type
	 * @return Observable
	 * @since 1.0.0
	 */
	<T> Observable<Boolean> deleteObservable(T entity);

	/**
	 * RxJava wrapper of {@link #delete(Collection)}
	 * @param entities collection of entities
	 * @return Observable
	 * @since 1.0.0
	 */
	Observable<Boolean> deleteObservable(Collection<?> entities);


	/**
	 * RxJava wrapper of {@link #sql(Class, String, String[], Property...)} )}
	 * @return Observable
	 * @param <T> entity type
	 * @param sql sql expression
	 * @param args args
	 * @param type entity type
	 * @param property properties
	 * @since 1.0.0
	 */
	<T> Observable<List<T>> sqlObservable(Class<T> type, String sql, String[] args, Property... property);


	/**
	 * RxJava wrapper of {@link #sqlEntityCursor(Class, String, String[], Property...)})}
	 * @return Observable
	 * @param <T> entity type
	 * @param sql sql expression
	 * @param args args
	 * @param type entity type
	 * @param property properties
	 * @since 1.0.0
	 */
	<T> Observable<EntityCursor<T>> sqlEntityCursorObservable(Class<T> type, String sql, String[] args, Property... property);

	/**
	 * RxJava wrapper of {@link #sqlFirstResult(Class, String, String[], Property...)}}
	 * @return Observable
	 * @param <T> entity type
	 * @param sql sql expression
	 * @param args args
	 * @param type entity type
	 * @param property properties
	 * @since 1.0.0
	 */
	<T> Observable<T> sqlFirstResultObservable(Class<T> type, String sql, String[] args, Property... property);
}
