/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.core.manager;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.bitbucket.txdrive.electra.annotation.Schema;
import org.bitbucket.txdrive.electra.core.converter.ColumnType;
import org.bitbucket.txdrive.electra.core.converter.FieldConverter;

import org.bitbucket.txdrive.electra.core.exception.ElectraException;
import org.bitbucket.txdrive.electra.core.query.EntityCursor;
import org.bitbucket.txdrive.electra.core.query.Expression;
import org.bitbucket.txdrive.electra.core.query.Func;
import org.bitbucket.txdrive.electra.core.query.Properties;
import org.bitbucket.txdrive.electra.core.query.Property;
import org.bitbucket.txdrive.electra.core.query.QueryContext;
import org.bitbucket.txdrive.electra.core.query.Restrictions;
import org.bitbucket.txdrive.electra.core.query.Select;
import org.bitbucket.txdrive.electra.core.query.Where;
import org.bitbucket.txdrive.electra.meta.FieldAccessor;
import org.bitbucket.txdrive.electra.meta.MetaField;
import org.bitbucket.txdrive.electra.meta.MetaType;
import org.bitbucket.txdrive.electra.meta.MetaTypes;
import org.bitbucket.txdrive.electra.meta.MetaUtils;
import org.bitbucket.txdrive.electra.utils.SqlUtils;

import rx.Observable;
import rx.functions.Func0;

abstract public class AbstractEntityManager implements EntityManager {

	@Override
	public void close() {
		getDatabase().close();
	}

	@Override
	public boolean isOpen() {
		return getDatabase().isOpen();
	}

	@Override
	public <T> void addEntityListener(Class<T> type, EntityListener<T> entityListener) {
		MetaType<T> metaType = MetaTypes.typeOf(type);
		metaType.addEntityListener(entityListener);
	}

	@Override
	public <T> void removeEntityListener(Class<T> type, EntityListener<T> entityListener) {
		MetaType<T> metaType = MetaTypes.typeOf(type);
		metaType.removeEntityListener(entityListener);
	}

	@Override
	public void createTables() {
		Map<Class, MetaType> types = MetaTypes.getTypes();
		for (Class<?> type : types.keySet()) {
			MetaType<?> metaType = types.get(type);
			createTable(metaType);
		}
	}

	@Override
	public <T> void createTable(Class<T> type) {
		MetaType<T> metaType = MetaTypes.typeOf(type);
		createTable(metaType);
	}

	private void createTable(MetaType<?> metaType) {
		Schema[] schema = metaType.getSchema();

		if (MetaUtils.isSchemaPresent(schema, Schema.CREATE)) {
			CreateTableExpression expression = new CreateTableExpression(metaType);
			getDatabase().execSQL(expression.toSql());

			CreateIndexesOperation createIndexesOperation = new CreateIndexesOperation(metaType, this);
			createIndexesOperation.execute();
		}
	}

	@Override
	public void updateTables() {
		Map<Class, MetaType> types = MetaTypes.getTypes();
		for (Class<?> type : types.keySet()) {
			MetaType<?> metaType = types.get(type);

			updateTable(metaType);
		}
	}

	@Override
	public <T> void updateTable(Class<T> type) {
		MetaType<T> metaType = MetaTypes.typeOf(type);
		updateTable(metaType);
	}

	private void updateTable(MetaType<?> metaType) {
		Schema[] schema = metaType.getSchema();

		if (MetaUtils.isSchemaPresent(schema, Schema.UPDATE)) {
			DropIndexesOperation dropIndexesOperation = new DropIndexesOperation(metaType, this);
			dropIndexesOperation.execute();

			UpdateTableOperation updateTableOperation = new UpdateTableOperation(metaType, this);
			updateTableOperation.execute();

			CreateIndexesOperation createIndexesOperation = new CreateIndexesOperation(metaType, this);
			createIndexesOperation.execute();
		}
	}

	@Override
	public void dropTables() {
		Map<Class, MetaType> types = MetaTypes.getTypes();
		for (Class<?> type : types.keySet()) {
			MetaType<?> metaType = types.get(type);
			dropTable(metaType);
		}
	}

	@Override
	public <T> void dropTable(Class<T> type) {
		MetaType<T> metaType = MetaTypes.typeOf(type);
		dropTable(metaType);
	}

	private void dropTable(MetaType<?> metaType) {
		Schema[] schema = metaType.getSchema();

		if (MetaUtils.isSchemaPresent(schema, Schema.DROP)) {
			DropTableExpression expression = new DropTableExpression(metaType);
			getDatabase().execSQL(expression.toSql());
		}
	}


	@Override
	public <T> Select<T> select(Class<T> type, Property... property) {
		return new Select<>(this, type, property);
	}

	@Override
	public <T> boolean save(T entity) {
		boolean result;

		MetaType<T> metaType = MetaTypes.typeOf(entity.getClass());
		List<MetaField> keyFields = MetaUtils.getKeyFields(metaType);

		if (keyFields.isEmpty()) {
			throw new ElectraException("Save cannot be applied to Entity without primary key(s)");
		}

		Object[] keyValues = MetaUtils.getFieldValues(entity, keyFields);

		if (exists(entity.getClass(), keyValues)) {
			result = update(entity);
		} else {
			long rowId = create(entity);
			result = rowId > -1;
		}

		return result;
	}

	@Override
	public boolean save(Collection<?> entities) {
		boolean result = false;

		ElectraDatabase db = getDatabase();
		db.beginTransaction();

		try {

			for (Object entity : entities) {
				save(entity);
			}

			db.setTransactionSuccessful();
			result = true;
		} finally {
			db.endTransaction();
		}

		return result;
	}

	@Override
	public <T> boolean exists(Class<T> type, Object... key) {
		List<MetaField> keyFields = MetaUtils.getKeyFields(MetaTypes.typeOf(type));

		return exists(type, SqlUtils.getExpression(keyFields, key));
	}

	@Override
	public <T> boolean exists(Class<T> type, Expression... expressions) {
		boolean result = false;

		Cursor cursor = select(type, Properties.rowCount())
				.max(1)
				.where(Restrictions.and(expressions)).cursor();

		if (cursor != null && cursor.moveToNext()) {
			long count = cursor.getLong(0);
			result = count > 0;
		}

		if (cursor != null) {
			cursor.close();
		}

		return result;
	}

	@Override
	public <T> long create(T entity) {
		ElectraDatabase db = getDatabase();

		MetaType<T> metaType = MetaTypes.typeOf(entity.getClass());

		metaType.notifyPreCreate(entity);

		List<MetaField> keyFields = MetaUtils.getKeyFields(metaType);
		MetaField autoPrimaryKey = findAutoPrimaryKey(keyFields);

		ContentValues cv = new ContentValues();
		for (MetaField metaField : MetaUtils.getEditFields(metaType)) {
			if (metaField.isKey() && autoPrimaryKey != null) {
				continue;
			}

			FieldAccessor fieldAccessor = metaField.getAccessor();
			Object value = fieldAccessor.get(entity);

			if (value != null) {
				FieldConverter fieldConverter = metaField.getConverter();
				fieldConverter.populateContentValues(metaField.getColumnName(), value, cv);
			}
		}

		long rowId = db.insert(metaType.getTableName(), cv);

		if (autoPrimaryKey != null) {
			autoPrimaryKey.getAccessor().set(entity, rowId);
		}

		if (rowId > 0) {
			metaType.notifyPostCreate(entity);
		}

		return rowId;
	}

	@Override
	public boolean create(Collection<?> entities) {
		boolean result = false;

		ElectraDatabase db = getDatabase();
		db.beginTransaction();

		try {
			for (Object entity : entities) {
				create(entity);
			}
			db.setTransactionSuccessful();

			result = true;
		} finally {
			db.endTransaction();
		}

		return result;
	}

	@Override
	public <T> T read(Class<T> type, Object... key) {
		MetaType<T> metaType = MetaTypes.typeOf(type);

		List<MetaField> keyFields = MetaUtils.getKeyFields(metaType);
		Expression expression = SqlUtils.getExpression(keyFields, key);

		T entity = select(type).where(expression).first();

		if (entity != null) {
			metaType.notifyPostRead(entity);
		}

		return entity;
	}

	@Override
	public <T> boolean update(T entity) {
		ElectraDatabase db = getDatabase();

		MetaType<T> metaType = MetaTypes.typeOf(entity.getClass());

		List<MetaField> keyFields = MetaUtils.getKeyFields(metaType);
		if (keyFields.size() == 0) {
			throw new ElectraException("Key(s) field not found for " + metaType.getName());
		}

		Expression keyExpression = SqlUtils.getExpressionByFields(entity, keyFields);

		QueryContext queryContext = new QueryContext(metaType);
		keyExpression.resolveNames(queryContext);

		List<MetaField> updateFields = MetaUtils.getEditFieldsSkipKeyFields(metaType);

		ContentValues cv = new ContentValues();
		for (MetaField metaField : updateFields) {
			Object value = metaField.getAccessor().get(entity);
			if (value != null) {
				metaField.getConverter().populateContentValues(metaField.getColumnName(), value, cv);
			}
		}

		metaType.notifyPreUpdate(entity);
		long count = db.update(metaType.getTableName(), cv, keyExpression.toSql());

		boolean isUpdated = count > 0;

		if (isUpdated) {
			metaType.notifyPostUpdate(entity);
		}

		return isUpdated;
	}

	@Override
	public boolean update(Collection<?> entities) {
		boolean result = false;
		ElectraDatabase db = getDatabase();
		db.beginTransaction();

		try {
			for (Object entity : entities) {
				update(entity);
			}
			db.setTransactionSuccessful();
			result = true;
		} finally {
			db.endTransaction();
		}
		return result;
	}

	@Override
	public <T> boolean delete(T entity) {
		ElectraDatabase db = getDatabase();

		MetaType<T> metaType = MetaTypes.typeOf(entity.getClass());

		List<MetaField> keyFields = MetaUtils.getKeyFields(metaType);
		if (keyFields.size() == 0) {
			throw new ElectraException("Key(s) field not found for " + metaType.getName());
		}

		Expression keyExpression = SqlUtils.getExpressionByFields(entity, keyFields);

		QueryContext queryContext = new QueryContext(metaType);
		keyExpression.resolveNames(queryContext);

		metaType.notifyPreDelete(entity);
		long count = db.delete(metaType.getTableName(), keyExpression.toSql());

		boolean isDeleted = count > 0;

		if (isDeleted) {
			metaType.notifyPostDelete(entity);
		}

		return isDeleted;
	}

	@Override
	public boolean delete(Collection<?> entities) {
		boolean result = false;
		ElectraDatabase db = getDatabase();
		db.beginTransaction();

		try {
			for (Object entity : entities) {
				delete(entity);
			}

			db.setTransactionSuccessful();
			result = true;
		} finally {
			db.endTransaction();
		}

		return result;
	}

	private MetaField findAutoPrimaryKey(List<MetaField> keyFields) {
		MetaField result = null;

		if (keyFields.size() == 1) {
			MetaField metaField = keyFields.get(0);
			if (metaField.getConverter().getType().equals(ColumnType.INTEGER)) {
				result = metaField;
			}
		}

		return result;
	}

	@Override
	public <T> Where<T> where(Class<T> type, Expression... where) {
		return new Where<>(this, type, where);
	}

	@Override
	public <T> Func<T> func(Class<T> type) {
		return new Func<>(type, this, null);
	}


	@Override
	public <T> EntityCursor<T> sqlEntityCursor(Class<T> type, String sql, String[] args, Property... property) {
		MetaType<T> metaType = MetaTypes.typeOf(type);

		QueryContext queryContext = new QueryContext(metaType);

		String sqlResolved = queryContext.resolveSql(sql);
		Cursor cursor = getDatabase().rawQuery(sqlResolved, args);


		Property[] selectProperties = property;
		if (!SqlUtils.hasValues(property)) {
			selectProperties = new Property[]{Properties.all()};
		}

		return (cursor != null) ?
				new EntityCursor<>(cursor, metaType, selectProperties) : null;
	}

	@Override
	public <T> List<T> sql(Class<T> type, String sql, String[] arg, Property... property) {
		List<T> result = new ArrayList<>();
		EntityCursor<T> entityCursor = sqlEntityCursor(type, sql, arg, property);

		if (entityCursor != null) {
			while (entityCursor.moveToNext()) {
				result.add(entityCursor.get());
			}

			entityCursor.close();
		}

		return result;
	}

	@Override
	public <T> T sqlFirstResult(Class<T> type, String sql, String[] args,  Property... property) {
		List<T> result = sql(type, sql, args, property);
		return !result.isEmpty() ? result.get(0) : null;
	}

	@Override
	public Observable<Void> closeObservable() {

		return Observable.defer(new Func0<Observable<Void>>() {
			@Override
			public Observable<Void> call() {
				close();
				return Observable.just(null);
			}
		});
	}

	@Override
	public Observable<Boolean> isOpenObservable() {

		return Observable.defer(new Func0<Observable<Boolean>>() {
			@Override
			public Observable<Boolean> call() {
				return Observable.just(isOpen());
			}
		});
	}

	@Override
	public <T> Observable<Boolean> existsObservable(final Class<T> type, final Object... key) {

		return Observable.defer(new Func0<Observable<Boolean>>() {
			@Override
			public Observable<Boolean> call() {
				return Observable.just(exists(type, key));
			}
		});
	}

	@Override
	public <T> Observable<Boolean> existsObservable(final Class<T> type, final Expression... where) {

		return Observable.defer(new Func0<Observable<Boolean>>() {
			@Override
			public Observable<Boolean> call() {
				return Observable.just(exists(type, where));
			}
		});
	}

	@Override
	public <T> Observable<Boolean> saveObservable(final T entity) {

		return Observable.defer(new Func0<Observable<Boolean>>() {
			@Override
			public Observable<Boolean> call() {
				return Observable.just(save(entity));
			}
		});
	}

	@Override
	public Observable<Boolean> saveObservable(final Collection<?> entities) {

		return Observable.defer(new Func0<Observable<Boolean>>() {
			@Override
			public Observable<Boolean> call() {
				return Observable.just(save(entities));
			}
		});
	}

	@Override
	public <T> Observable<Long> createObservable(final T entity) {

		return Observable.defer(new Func0<Observable<Long>>() {
			@Override
			public Observable<Long> call() {
				return Observable.just(create(entity));
			}
		});
	}

	@Override
	public Observable<Boolean> createObservable(final Collection<?> entities) {

		return Observable.defer(new Func0<Observable<Boolean>>() {
			@Override
			public Observable<Boolean> call() {
				return Observable.just(create(entities));
			}
		});
	}

	@Override
	public <T> Observable<T> readObservable(final Class<T> type, final Object... key) {

		return Observable.defer(new Func0<Observable<T>>() {
			@Override
			public Observable<T> call() {
				return Observable.just(read(type, key));
			}
		});
	}

	@Override
	public <T> Observable<Boolean> updateObservable(final T entity) {

		return Observable.defer(new Func0<Observable<Boolean>>() {
			@Override
			public Observable<Boolean> call() {
				return Observable.just(update(entity));
			}
		});
	}

	@Override
	public Observable<Boolean> updateObservable(final Collection<?> entities) {

		return Observable.defer(new Func0<Observable<Boolean>>() {
			@Override
			public Observable<Boolean> call() {
				return Observable.just(update(entities));
			}
		});
	}

	@Override
	public <T> Observable<Boolean> deleteObservable(final T entity) {

		return Observable.defer(new Func0<Observable<Boolean>>() {
			@Override
			public Observable<Boolean> call() {
				return Observable.just(delete(entity));
			}
		});
	}

	@Override
	public Observable<Boolean> deleteObservable(final Collection<?> entities) {

		return Observable.defer(new Func0<Observable<Boolean>>() {
			@Override
			public Observable<Boolean> call() {
				return Observable.just(delete(entities));
			}
		});
	}

	@Override
	public <T> Observable<List<T>> sqlObservable(final Class<T> type, final String sql,
	                                             final String[] args, final Property... property) {
		return Observable.defer(new Func0<Observable<List<T>>>() {
			@Override
			public Observable<List<T>> call() {
				return Observable.just(sql(type, sql, args, property));
			}
		});
	}

	@Override
	public <T> Observable<EntityCursor<T>> sqlEntityCursorObservable(final Class<T> type,
	                                                                 final String sql, final String[] args, final Property... property) {
		return Observable.defer(new Func0<Observable<EntityCursor<T>>>() {
			@Override
			public Observable<EntityCursor<T>> call() {
				return Observable.just(sqlEntityCursor(type, sql, args, property));
			}
		});
	}

	@Override
	public <T> Observable<T> sqlFirstResultObservable(final Class<T> type, final String sql,
	                                                  final String[] args, final Property... property) {
		return Observable.defer(new Func0<Observable<T>>() {
			@Override
			public Observable<T> call() {
				return Observable.just(sqlFirstResult(type, sql, args, property));
			}
		});
	}
}
