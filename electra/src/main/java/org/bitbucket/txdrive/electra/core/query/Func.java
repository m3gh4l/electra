/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.core.query;

import org.bitbucket.txdrive.electra.core.converter.FieldConverter;

import org.bitbucket.txdrive.electra.core.converter.FieldConverters;
import org.bitbucket.txdrive.electra.core.manager.EntityManager;
import org.bitbucket.txdrive.electra.meta.MetaField;
import org.bitbucket.txdrive.electra.meta.MetaType;
import org.bitbucket.txdrive.electra.meta.MetaTypes;
import org.bitbucket.txdrive.electra.meta.MetaUtils;
import rx.Observable;
import rx.functions.Func0;

public class Func<T> {
	private EntityManager em;
	private Expression where;
	private Class<T> type;
	private MetaType<T> metaType;


	public Func(Class<T> type, EntityManager em, Expression where) {
		this.em = em;
		this.where = where;
		this.type = type;
		this.metaType = MetaTypes.typeOf(type);
	}

	private Object getResult(Property property, FieldConverter fieldConverter) {
		Select select = em.select(type, property);

		if (where != null) {
			select.where(where);
		}

		Object[] result = select.firstResult(fieldConverter);

		return result != null ? result[0] : null;
	}


	public Object min(String name) {
		MetaField metaField = MetaUtils.findMetaField(metaType, name);
		return getResult(Properties.min(name), metaField.getConverter());
	}

	public Object max(String name) {
		MetaField metaField = MetaUtils.findMetaField(metaType, name);
		return getResult(Properties.max(name), metaField.getConverter());
	}

	public Object avg(String name) {
		MetaField metaField = MetaUtils.findMetaField(metaType, name);
		return getResult(Properties.avg(name), metaField.getConverter());
	}

	public Object sum(String name) {
		MetaField metaField = MetaUtils.findMetaField(metaType, name);
		return getResult(Properties.sum(name), metaField.getConverter());
	}

	public Long count(String name) {
		return (Long) getResult(Properties.count(name), new FieldConverters.LongConverter());
	}

	public Long rowCount() {
		return (Long) getResult(Properties.rowCount(), new FieldConverters.LongConverter());
	}

	public Observable<Object> minObservable(final String name) {

		return Observable.defer(new Func0<Observable<Object>>() {
			@Override
			public Observable<Object> call() {
				return Observable.just(min((name)));
			}
		});
	}

	public Observable<Object> maxObservable(final String name) {
		return Observable.defer(new Func0<Observable<Object>>() {
			@Override
			public Observable<Object> call() {
				return Observable.just(max((name)));
			}
		});
	}

	public Observable<Object> avgObservable(final String name) {
		return Observable.defer(new Func0<Observable<Object>>() {
			@Override
			public Observable<Object> call() {
				return Observable.just(avg((name)));
			}
		});
	}

	public Observable<Object> sumObservable(final String name) {
		return Observable.defer(new Func0<Observable<Object>>() {
			@Override
			public Observable<Object> call() {
				return Observable.just(sum((name)));
			}
		});
	}

	public Observable<Long> countObservable(final String name) {
		return Observable.defer(new Func0<Observable<Long>>() {
			@Override
			public Observable<Long> call() {
				return Observable.just(count((name)));
			}
		});
	}

	public Observable<Long> countRowObservable() {
		return Observable.defer(new Func0<Observable<Long>>() {
			@Override
			public Observable<Long> call() {
				return Observable.just(rowCount());
			}
		});
	}

}
