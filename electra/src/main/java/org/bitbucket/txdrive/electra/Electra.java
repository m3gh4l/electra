/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bitbucket.txdrive.electra.core.manager.DatabaseManagerCustom;
import org.bitbucket.txdrive.electra.core.manager.DatabaseManagerWithHelper;
import org.bitbucket.txdrive.electra.core.manager.ElectraDatabase;
import org.bitbucket.txdrive.electra.core.manager.DatabaseManagerDefault;
import org.bitbucket.txdrive.electra.core.manager.EntityManager;
import org.bitbucket.txdrive.electra.meta.MetaType;
import org.bitbucket.txdrive.electra.meta.MetaTypes;

public class Electra {

	private Electra() {

	}

	/**
	 * Register entities
	 * @since 1.0.0
	 * @param metaTypes list of the meta types. Basically it is auto generated class "ED". ED.TYPES
	 */
	public static void configure(List<MetaType> metaTypes) {
		Map<Class, MetaType> types = new HashMap<>();

		for (MetaType<?> metaType : metaTypes) {
			Class<?> type = metaType.getType();
			types.put(type, metaType);
		}

		MetaTypes.setTypes(types);
	}

	/**
	 *  Get Entity manager
	 * @since 1.0.0
	 * @param database sqlite data base
	 * @return EntityManager
	 */
	public static EntityManager with(SQLiteDatabase database) {
		return new DatabaseManagerDefault(database);
	}

	/**
	 *  Get Entity manager
	 * @since 1.0.0
	 * @param helper sqlite open helper
	 * @return EntityManager
	 */
	public static EntityManager with(SQLiteOpenHelper helper) {
		return new DatabaseManagerWithHelper(helper);
	}

	/**
	 *  Get Entity manager by custom db wrapper
	 * @since 1.0.0
	 * @param database custom wrapper of db
	 * @return EntityManager
	 */
	public static EntityManager with(ElectraDatabase database) {
		return new DatabaseManagerCustom(database);
	}
}
