/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.core.query;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.txdrive.electra.core.converter.FieldConverter;
import org.bitbucket.txdrive.electra.core.manager.AbstractEntityManager;
import org.bitbucket.txdrive.electra.meta.MetaField;
import org.bitbucket.txdrive.electra.meta.MetaTypes;
import org.bitbucket.txdrive.electra.utils.SqlUtils;
import org.bitbucket.txdrive.electra.utils.StringUtils;

import rx.Observable;
import rx.functions.Func0;

public class Select<T> {
	private org.bitbucket.txdrive.electra.meta.MetaType<T> metaType;
	private org.bitbucket.txdrive.electra.core.manager.AbstractEntityManager manager;
	private QueryContext queryContext;

	private boolean distinct;
	private Property[] properties;
	private Expression where;
	private String[] groupBy;
	private String having;
	private Order[] orders;

	private long position;
	private long maxResult;

	public Select(AbstractEntityManager manager, Class<T> type, Property... property) {
		this.manager = manager;
		this.metaType = MetaTypes.typeOf(type);
		this.queryContext = new QueryContext(metaType);
		this.properties = property;
	}

	/**
	 * Apply distinct
	 * @since 1.0.0
	 * @return this (for method chaining)
	 */
	public Select<T> distinct() {
		distinct = true;
		return this;
	}

	/**
	 * Add order
	 * @since 1.0.0
	 * @param order The orders objects.
	 * @return this (for method chaining)
	 */
	public Select<T> order(Order... order) {
		this.orders = order;

		return this;
	}

	/**
	 * Add a restriction to constrain the results.
	 * @since 1.0.0
	 * @param expression The expression objects representing the restriction to be applied.
	 * @return this (for method chaining)
	 */
	public Select<T> where(Expression... expression) {
		where = Restrictions.and(expression);

		return this;
	}

	/**
	 * Add group by
	 * @since 1.0.0
	 * @param name Groping field names
	 * @return this (for method chaining)
	 */
	public Select<T> groupBy(String... name) {
		this.groupBy = name;
		return this;
	}

	/**
	 * Add having
	 * @since 1.0.0
	 * @param sql The SQL having
	 * @return this (for method chaining)
	 */
	public Select<T> having(String sql) {
		this.having = sql;
		return this;
	}

	/**
	 * Set the first result to be retrieved.
	 * @since 1.0.0
	 * @param position the first result to retrieve
	 * @return this (for method chaining)
	 */
	public Select<T> from(long position) {
		this.position = position;

		return this;
	}

	/**
	 * Set a limit result of objects to be retrieved.
	 * @since 1.0.0
	 * @param maxResult the maximum number of results
	 * @return this (for method chaining)
	 */
	public Select<T> max(long maxResult) {
		this.maxResult = maxResult;

		return this;
	}

	/**
	 * Get the results.
	 * @since 1.0.0
	 * @return The list of matched query results.
	 */
	public List<T> list() {
		List<T> result = new ArrayList<>();

		Cursor cursor = cursor();

		EntityCursor<T> entityCursor = (cursor != null) ?
				new EntityCursor<>(cursor, metaType, properties) : null;

		if (entityCursor != null) {
			while (entityCursor.moveToNext()) {
				result.add(entityCursor.get());
			}

			entityCursor.close();
		}

		return result;
	}

	/**
	 * Get the results as list of Object[] with custom converters
	 * @since 1.0.0
	 * @param converters field converters. Field converters must have the order
	 * the same with field order of the entity or order of properties
	 * @return The list of matched query results.
	 */
	public List<Object[]> listResult(FieldConverter... converters) {
		List<Object[]> result = new ArrayList<>();

		Cursor cursor = cursor();

		if (cursor != null) {
			while (cursor.moveToNext()) {
				Object[] values = new Object[converters.length];

				int idx = 0;
				for (FieldConverter converter : converters) {
					Object value = converter.fromCursor(cursor, idx);
					values[idx] = value;
					idx++;
				}

				result.add(values);
			}

			cursor.close();
		}

		return result;

	}

	/**
	 * Get the results as list of Object[]
	 * @since 1.0.0
	 * @return The list of matched query results.
	 */
	public List<Object[]> listResult() {
		List<Object[]> result = new ArrayList<>();

		Cursor cursor = cursor();
		List<MetaField<T, ?>> metaFields = SqlUtils
				.filterByProperties(metaType.getMetaFields(), properties);

		if (cursor != null) {
			while (cursor.moveToNext()) {
				Object[] values = new Object[metaFields.size()];

				int idx = 0;
				for (MetaField metaField : metaFields) {
					int columnIndex = cursor.getColumnIndex(metaField.getColumnName());
					if (columnIndex > -1) {
						FieldConverter fieldConverter = metaField.getConverter();
						Object value = fieldConverter.fromCursor(cursor, columnIndex);
						values[idx] = value;
					}
					idx++;
				}


				result.add(values);
			}

			cursor.close();
		}


		return result;
	}

	/**
	 * Get the results of entity field
	 * @since 1.0.0
	 * @param fieldName field name
	 * @param <V> field type
	 * @return The list of matched query results.
	 */
	public <V> List<V> listFieldValues(String fieldName) {
		List<V> result = new ArrayList<>();
		MetaField<T, ?> metaField = SqlUtils.getMetaFieldByName(metaType.getMetaFields(), fieldName);

		Cursor cursor = cursor();
		if (cursor != null) {
			while (cursor.moveToNext()) {

				int columnIndex = cursor.getColumnIndex(metaField.getColumnName());
				if (columnIndex > -1) {
					FieldConverter fieldConverter = metaField.getConverter();
					Object value = fieldConverter.fromCursor(cursor, columnIndex);
					result.add((V) value);
				}
			}

			cursor.close();
		}


		return result;
	}

	/**
	 * Get results as entity cursor wrapper
	 * @since 1.0.0
	 * @return EntityCursor
	 */
	public EntityCursor<T> entityCursor() {
		Cursor cursor = cursor();
		return (cursor != null) ? new EntityCursor<>(cursor, metaType, properties) : null;
	}

	/**
	 * Get first result
	 * @since 1.0.0
	 * @return entity, null if not found
	 */
	public T first() {
		List<T> result = list();
		return !result.isEmpty() ? result.get(0) : null;
	}

	/**
	 * Get first result as Object[]
	 * @since 1.0.0
	 * @return Object[], null if not found
	 */
	public Object[] firstResult() {
		List<Object[]> result = listResult();
		return !result.isEmpty() ? result.get(0) : null;
	}

	/**
	 * Get first result as Object[] with custom field converters
	 * @since 1.0.0
	 * @param fieldConverters field converters. Field converters must have the order
	 * the same with field order of the entity or order of properties
	 * @return Object[], null if not found
	 */
	public Object[] firstResult(FieldConverter... fieldConverters) {
		List<Object[]> result = listResult(fieldConverters);
		return !result.isEmpty() ? result.get(0) : null;
	}

	/**
	 * Get results as cursor
	 * @since 1.0.0
	 * @return Cursor
	 */
	public Cursor cursor() {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT");

		if (distinct) {
			sql.append(" DISTINCT");
		}

		sql.append(" ");

		if (!SqlUtils.hasValues(properties)) {
			properties = new Property[]{ Properties.all() };
		}

		sql.append(SqlUtils.sqlCSV(queryContext, properties));


		sql.append(" FROM ").append(metaType.getTableName());

		if (where != null) {
			where.resolveNames(queryContext);
			sql.append(" WHERE ").append(where.toSql());
		}

		if (SqlUtils.hasValues(groupBy)) {
			sql.append(" GROUP BY ").append(SqlUtils.sqlCSV(queryContext, groupBy));
		}

		if (!StringUtils.isEmpty(having)) {
			sql.append(" HAVING ").append(queryContext.resolveSql(having));
		}

		if (SqlUtils.hasValues(orders)) {
			sql.append(" ORDER BY ").append(SqlUtils.sqlCSV(queryContext, orders));
		}

		String limitSql = SqlUtils.toSqlLimit(position, maxResult);
		if (limitSql != null) {
			sql.append(" LIMIT ").append(limitSql);
		}

		return manager.getDatabase().rawQuery(sql.toString(), null);
	}

	/**
	 * Get function query builder
	 * @since 1.0.0
	 * @return Function query builder
	 * @see Expression
	 */
	public Func<T> func() {
		return new Func<>(metaType.getType(), manager, where);
	}

	/**
	 * RxJava wrapper of {@link #list()}
	 * @return Observable
	 * @since 1.0.0
	 */
	public Observable<List<T>> listObservable() {

		return Observable.defer(new Func0<Observable<List<T>>>() {
			@Override
			public Observable<List<T>> call() {
				return Observable.just(list());
			}
		});
	}

	/**
	 * RxJava wrapper of {@link #listResult(FieldConverter[])}
	 * @param converters field converters
	 * @return Observable
	 * @since 1.0.0
	 */
	public Observable<List<Object[]>> listResultObservable(final FieldConverter... converters) {

		return Observable.defer(new Func0<Observable<List<Object[]>>>() {
			@Override
			public Observable<List<Object[]>> call() {
				return Observable.just(listResult(converters));
			}
		});
	}

	/**
	 * RxJava wrapper of {@link #listResult()}
	 * @return Observable
	 * @since 1.0.0
	 */
	public Observable<List<Object[]>> listResultObservable() {

		return Observable.defer(new Func0<Observable<List<Object[]>>>() {
			@Override
			public Observable<List<Object[]>> call() {
				return Observable.just(listResult());
			}
		});
	}

	/**
	 * RxJava wrapper of {@link #listFieldValues(String)} ()}
	 * @param fieldName field name
	 * @param <V> field type
	 * @return Observable
	 * @since 1.0.0
	 */
	public <V> Observable<List<V>> listFieldValuesObservable(final String fieldName) {

		return Observable.defer(new Func0<Observable<List<V>>>() {
			@Override
			public Observable<List<V>> call() {
				return Observable.just((List<V>) listFieldValues(fieldName));
			}
		});
	}

	/**
	 * RxJava wrapper of {@link #entityCursor()}
	 * @return Observable
	 * @since 1.0.0
	 */
	public Observable<EntityCursor<T>> entityCursorObservable() {

		return Observable.defer(new Func0<Observable<EntityCursor<T>>>() {
			@Override
			public Observable<EntityCursor<T>> call() {
				return Observable.just(entityCursor());
			}
		});
	}

	/**
	 * RxJava wrapper of {@link #firstResult()}
	 * @return Observable
	 * @since 1.0.0
	 */
	public Observable<T> firstObservable() {

		return Observable.defer(new Func0<Observable<T>>() {
			@Override
			public Observable<T> call() {
				return Observable.just(first());
			}
		});
	}

	/**
	 * RxJava wrapper of {@link #firstResult()}
	 * @return Observable
	 * @since 1.0.0
	 */
	public Observable<Object[]> firstResultObservable() {
		return Observable.defer(new Func0<Observable<Object[]>>() {

			@Override
			public Observable<Object[]> call() {
				return Observable.just(firstResult());
			}
		});
	}

	/**
	 * RxJava wrapper of {@link #firstResult(FieldConverter[])}
	 * @param fieldConverters field converters
	 * @return Observable
	 * @since 1.0.0
	 */
	public Observable<Object[]> firstResultObservable(final FieldConverter... fieldConverters) {
		return Observable.defer(new Func0<Observable<Object[]>>() {

			@Override
			public Observable<Object[]> call() {
				return Observable.just(firstResult(fieldConverters));
			}
		});
	}


	/**
	 * RxJava wrapper of {@link #cursor()}
	 * @return Observable
	 * @since 1.0.0
	 */
	public Observable<Cursor> cursorObservable() {
		return Observable.defer(new Func0<Observable<Cursor>>() {
			@Override
			public Observable<Cursor> call() {
				return Observable.just(cursor());
			}
		});
	}


}

