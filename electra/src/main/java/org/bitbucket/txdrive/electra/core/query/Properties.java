/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.core.query;

public class Properties {

	private Properties() {

	}

	/**
	 * Property name
	 * @since 1.0.0
	 * @param name field name
	 * @return Property
	 */
	public static Property name(String name) {
		return new NameProperty(name);
	}

	/**
	 * Property *
	 * @since 1.0.0
	 * @return Property
	 */
	public static Property all() {
		return new AllProperty();
	}

	/**
	 * Property as sql, you can use :field_name: to resolve db column name
	 * @since 1.0.0
	 * @param sql sql property
	 * @return Property
	 */
	public static Property sql(String sql) {
		return new SqlProperty(sql);
	}

	/**
	 * Property avg
	 * @since 1.0.0
	 * @param name field name
	 * @return Property
	 */
	public static Property avg(String name) {
		return new FuncProperty(name, "AVG");
	}

	/**
	 * Property sum
	 * @since 1.0.0
	 * @param name field name
	 * @return Property
	 */
	public static Property sum(String name) {
		return new FuncProperty(name, "SUM");
	}

	/**
	 * Property max
	 * @since 1.0.0
	 * @param name field name
	 * @return Property
	 */
	public static Property max(String name) {
		return new FuncProperty(name, "MAX");
	}

	/**
	 * Property min
	 * @since 1.0.0
	 * @param name field name
	 * @return Property
	 */
	public static Property min(String name) {
		return new FuncProperty(name, "MIN");
	}

	/**
	 * Property count
	 * @since 1.0.0
	 * @param name field name
	 * @return Property
	 */
	public static Property count(String name) {
		return new FuncProperty(name, "COUNT");
	}

	/**
	 * Property count(*)
	 * @since 1.0.0
	 * @return Property
	 */
	public static Property rowCount() {
		return new SqlProperty("COUNT(*)");
	}

	/**
	 * Create alias
	 * @since 1.0.0
	 * @param property property
	 * @param alias alias field name
	 * @return Property
	 */
	public static Property as(Property property, String alias) {
		return new AliasProperty(property, alias);
	}

	private static class AliasProperty implements Property {
		private Property property;
		private String alias;
		private String aliasResolved;

		public AliasProperty(Property property, String alias) {
			this.property = property;
			this.alias = alias;
		}

		@Override
		public String getName() {
			return alias;
		}

		@Override
		public void resolveNames(QueryContext context) {
			property.resolveNames(context);

			if (aliasResolved == null) {
				aliasResolved = context.resolveName(alias);
			}
		}

		@Override
		public String toSql() {
			return property.toSql() + " as " + aliasResolved;
		}
	}

	private static class AllProperty implements Property {

		@Override
		public String getName() {
			return "*";
		}

		@Override
		public void resolveNames(QueryContext context) {

		}

		@Override
		public String toSql() {
			return "*";
		}
	}

	private static class NameProperty implements Property {
		protected String name;
		protected String columnName;

		public NameProperty(String name) {
			this.name = name;
		}

		@Override
		public void resolveNames(QueryContext context) {
			if (columnName == null) {
				columnName = context.resolveName(name);
			}
		}

		@Override
		public String toSql() {
			return columnName;
		}

		@Override
		public String getName() {
			return name;
		}
	}

	private static class FuncProperty extends NameProperty {
		private String func;

		public FuncProperty(String name, String func) {
			super(name);
			this.func = func;
		}

		@Override
		public String toSql() {
			return func + "(" + columnName + ")";
		}

		@Override
		public String getName() {
			return null;
		}
	}

	private static class SqlProperty implements Property {
		private String sql;

		public SqlProperty(String sql) {
			this.sql = sql;
		}

		@Override
		public void resolveNames(QueryContext context) {
			sql = context.resolveSql(sql);
		}

		@Override
		public String toSql() {
			return sql;
		}

		@Override
		public String getName() {
			return null;
		}
	}


}
