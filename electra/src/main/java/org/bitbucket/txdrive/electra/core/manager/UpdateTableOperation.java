/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.core.manager;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.txdrive.electra.meta.MetaField;
import org.bitbucket.txdrive.electra.meta.MetaUtils;
import org.bitbucket.txdrive.electra.utils.SqlUtils;

public class UpdateTableOperation {
	private org.bitbucket.txdrive.electra.meta.MetaType<?> metaType;
	private ElectraDatabase db;
	private EntityManager em;

	public UpdateTableOperation(org.bitbucket.txdrive.electra.meta.MetaType<?> metaType, EntityManager manager) {
		this.metaType = metaType;
		this.db = manager.getDatabase();
		this.em = manager;
	}

	public void execute() {

		Cursor cursor = db.rawQuery("pragma table_info(" +
				SqlUtils.quote(metaType.getTableName()) + ")", null);

		if (cursor != null) {
			boolean exists = cursor.getCount() == 1;

			if (exists) {
				List<String> columnNames = getColumNames(cursor);
				cursor.close();
				updateTable(columnNames);
			} else {
				cursor.close();
				em.createTable(metaType.getType());
			}
		}

	}

	private List<String> getColumNames(Cursor cursor) {
		List<String> columnNames = new ArrayList<>();

		while (cursor.moveToNext()) {
			int idx = cursor.getColumnIndex("name");
			if (idx > -1) {
				String columnName = cursor.getString(idx);
				columnNames.add(columnName.toUpperCase());
			}
		}

		return columnNames;
	}

	private void updateTable(List<String> columnNames) {
		List<MetaField> newMetaFields = new ArrayList<>();

		for (MetaField metaField : MetaUtils.getEditFields(metaType)) {
			String columnName = metaField.getColumnName();
			if (!columnNames.contains(columnName.toUpperCase())) {
				newMetaFields.add(metaField);
			}
		}

		for (MetaField metaField : newMetaFields) {
			StringBuilder sql = new StringBuilder();
			sql.append("ALTER TABLE ").
					append(SqlUtils.quote(metaType.getTableName()))
					.append(" ADD COLUMN ")
					.append(SqlUtils.quote(metaField.getColumnName()))
					.append(" ")
					.append(metaField.getConverter().getType().toString());

			db.execSQL(sql.toString());
		}
	}
}
