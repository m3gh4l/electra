/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.core.query;

/**
 * Represents an ordering
 * @since 1.0.0
 */
public class Order implements NameResolveAware {
	private boolean asc;
	private String name;
	private String columnName;

	protected Order(boolean asc, String name) {
		this.asc = asc;
		this.name = name;
	}

	/**
	 * Ascending order
	 * @since 1.0.0
	 * @param name field name
	 * @return The build Order instance
	 */
	public static Order asc(String name) {
		return new Order(true, name);
	}

	/**
	 * Descending order
	 * @since 1.0.0
	 * @param name field name
	 * @return The build Order instance
	 */
	public static Order desc(String name) {
		return new Order(false, name);
	}

	@Override
	public void resolveNames(QueryContext context) {
		if (columnName == null) {
			columnName = context.resolveName(name);
		}
	}

	@Override
	public String toSql() {
		return columnName + " " + (asc ? "ASC" : "DESC");
	}
}
