/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.meta;

import org.bitbucket.txdrive.electra.core.converter.FieldConverter;
import org.bitbucket.txdrive.electra.utils.StringUtils;

public class MetaField<E, V> {
	private Class<V> type;
	private String name;
	private String columnName;
	private FieldAccessor<E, V> accessor;
	private FieldConverter<V> converter;
	private boolean key;
	private boolean auto;
	private boolean unique;
	private boolean notNull;
	private boolean readOnly;
	private org.bitbucket.txdrive.electra.meta.IndexMeta index;

	public static class MetaFieldBuilder<E, V> {
		private MetaField<E, V> metaField = new MetaField<>();

		public MetaField<E, V> build() {

			return metaField;
		}

		public MetaFieldBuilder<E, V> withConverter(FieldConverter<V> fieldConverter) {
			metaField.setConverter(fieldConverter);
			return this;
		}

		public MetaFieldBuilder<E, V> withType(Class<V> type) {
			metaField.setType(type);
			return this;
		}

		public MetaFieldBuilder<E, V> withName(String name) {
			metaField.setName(name);
			return this;
		}

		public MetaFieldBuilder<E, V> withColumnName(String columnName) {
			metaField.setColumnName(columnName);
			return this;
		}

		public MetaFieldBuilder<E, V> withAccessor(FieldAccessor<E, V> fieldAccessor) {
			metaField.setAccessor(fieldAccessor);
			return this;
		}

		public MetaFieldBuilder<E, V> withKey(boolean key) {
			metaField.setKey(key);
			return this;
		}

		public MetaFieldBuilder<E, V> withAuto(boolean auto) {
			metaField.setAuto(auto);
			return this;
		}

		public MetaFieldBuilder<E, V> withUnique(boolean unique) {
			metaField.setUnique(unique);
			return this;
		}

		public MetaFieldBuilder<E, V> withNotNull(boolean notNull) {
			metaField.setNotNull(notNull);
			return this;
		}

		public MetaFieldBuilder<E, V> withReadOnly(boolean readOnly) {
			metaField.setReadOnly(readOnly);
			return this;
		}


		public MetaFieldBuilder<E, V> withIndex(IndexMeta index) {
			metaField.setIndex(index);
			return this;
		}
	}

	public Class<V> getType() {
		return type;
	}

	public void setType(Class<V> type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColumnName() {
		if (StringUtils.isEmpty(columnName)) {
			columnName = name;
		}

		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public FieldAccessor<E, V> getAccessor() {
		return accessor;
	}

	public void setAccessor(FieldAccessor<E, V> accessor) {
		this.accessor = accessor;
	}

	public FieldConverter<V> getConverter() {
		return converter;
	}

	public void setConverter(FieldConverter<V> converter) {
		this.converter = converter;
	}

	public boolean isKey() {
		return key;
	}

	public void setKey(boolean key) {
		this.key = key;
	}

	public boolean isAuto() {
		return auto;
	}

	public void setAuto(boolean auto) {
		this.auto = auto;
	}

	public boolean isUnique() {
		return unique;
	}

	public void setUnique(boolean unique) {
		this.unique = unique;
	}

	public boolean isNotNull() {
		return notNull;
	}

	public void setNotNull(boolean notNull) {
		this.notNull = notNull;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public IndexMeta getIndex() {
		return index;
	}

	public void setIndex(IndexMeta index) {
		this.index = index;
	}
}
