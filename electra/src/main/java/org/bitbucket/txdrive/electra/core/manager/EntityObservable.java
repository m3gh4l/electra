/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.core.manager;

public interface EntityObservable<T> {
	void addEntityListener(EntityListener<T> entityListener);

	void removeEntityListener(EntityListener<T> entityListener);

	void notifyPreCreate(T entity);

	void notifyPostCreate(T entity);

	void notifyPostRead(T entity);

	void notifyPreUpdate(T entity);

	void notifyPostUpdate(T entity);

	void notifyPreDelete(T entity);

	void notifyPostDelete(T entity);

	void notifyChange(ChangeType changeType);
}
