/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.core.converter;

import android.content.ContentValues;
import android.database.Cursor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

public class FieldConverters {

	public static class BlobArrayConverter implements FieldConverter<byte[]> {
		@Override
		public byte[] fromCursor(Cursor cursor, int index) {
			return cursor.getBlob(index);
		}

		@Override
		public void populateContentValues(String key, byte[] value, ContentValues cv) {
			cv.put(key, value);
		}

		@Override
		public ColumnType getType() {
			return ColumnType.BLOB;
		}
	}

	public static class BooleanConverter implements FieldConverter<Boolean> {
		@Override
		public Boolean fromCursor(Cursor cursor, int index) {
			return cursor.getInt(index) == 1;
		}

		@Override
		public void populateContentValues(String key, Boolean value, ContentValues cv) {
			cv.put(key, value ? 1 : 0);
		}

		@Override
		public ColumnType getType() {
			return ColumnType.INTEGER;
		}
	}

	public static class StringConverter implements FieldConverter<String> {

		@Override
		public String fromCursor(Cursor cursor, int index) {
			return cursor.getString(index);
		}

		@Override
		public void populateContentValues(String key, String value, ContentValues cv) {
			cv.put(key, value);
		}

		@Override
		public ColumnType getType() {
			return ColumnType.TEXT;
		}
	}

	public static class ByteConverter implements FieldConverter<Byte> {

		@Override
		public Byte fromCursor(Cursor cursor, int index) {
			return (byte) cursor.getInt(index);
		}

		@Override
		public void populateContentValues(String key, Byte value, ContentValues cv) {
			cv.put(key, value);
		}

		@Override
		public ColumnType getType() {
			return ColumnType.INTEGER;
		}
	}

	public static class IntegerConverter implements FieldConverter<Integer> {

		@Override
		public Integer fromCursor(Cursor cursor, int index) {
			return cursor.getInt(index);
		}

		@Override
		public void populateContentValues(String key, Integer value, ContentValues cv) {
			cv.put(key, value);
		}

		@Override
		public ColumnType getType() {
			return ColumnType.INTEGER;
		}
	}

	public static class ShortConverter implements FieldConverter<Short> {

		@Override
		public Short fromCursor(Cursor cursor, int index) {
			return cursor.getShort(index);
		}

		@Override
		public void populateContentValues(String key, Short value, ContentValues cv) {
			cv.put(key, value);
		}

		@Override
		public ColumnType getType() {
			return ColumnType.INTEGER;
		}
	}

	public static class LongConverter implements FieldConverter<Long> {

		@Override
		public Long fromCursor(Cursor cursor, int index) {
			return cursor.getLong(index);
		}

		@Override
		public void populateContentValues(String key, Long value, ContentValues cv) {
			cv.put(key, value);
		}

		@Override
		public ColumnType getType() {
			return ColumnType.INTEGER;
		}
	}

	public static class FloatConverter implements FieldConverter<Float> {

		@Override
		public Float fromCursor(Cursor cursor, int index) {
			return cursor.getFloat(index);
		}

		@Override
		public void populateContentValues(String key, Float value, ContentValues cv) {
			cv.put(key, value);
		}

		@Override
		public ColumnType getType() {
			return ColumnType.REAL;
		}
	}

	public static class DoubleConverter implements FieldConverter<Double> {
		@Override
		public Double fromCursor(Cursor cursor, int index) {
			return cursor.getDouble(index);
		}

		@Override
		public void populateContentValues(String key, Double value, ContentValues cv) {
			cv.put(key, value);
		}

		@Override
		public ColumnType getType() {
			return ColumnType.REAL;
		}
	}

	public static class DateConverter implements FieldConverter<Date> {

		@Override
		public Date fromCursor(Cursor cursor, int index) {
			return cursor.isNull(index) ? null : new Date(cursor.getLong(index));
		}

		@Override
		public void populateContentValues(String key, Date value, ContentValues cv) {
			cv.put(key, value.getTime());
		}

		@Override
		public ColumnType getType() {
			return ColumnType.INTEGER;
		}
	}

	public static class BigDecimalConverter implements FieldConverter<BigDecimal> {

		@Override
		public BigDecimal fromCursor(Cursor cursor, int index) {
			return cursor.isNull(index) ? null : new BigDecimal(cursor.getString(index));
		}

		@Override
		public void populateContentValues(String key, BigDecimal value, ContentValues cv) {
			cv.put(key, value.toString());
		}

		@Override
		public ColumnType getType() {
			return ColumnType.TEXT;
		}
	}

	public static class BigIntegerConverter implements FieldConverter<BigInteger> {

		@Override
		public BigInteger fromCursor(Cursor cursor, int index) {
			return cursor.isNull(index) ? null : new BigInteger(cursor.getString(index));
		}

		@Override
		public void populateContentValues(String key, BigInteger value, ContentValues cv) {
			cv.put(key, value.toString());
		}

		@Override
		public ColumnType getType() {
			return ColumnType.TEXT;
		}
	}
}
