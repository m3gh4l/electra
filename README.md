# About Electra for android

Electra for android is simple persistence framework with compile time processing. 

Primary design goals:

* Easy to use and configure.
* Easy integrate with existing code. Electra like database wrapper. 
* Compile time processing. No reflection.
* Simple beans.
* Expression builder.
* RxJava support (optional).
* No Dependencies (RxJava optional)

![Electra.png](https://bitbucket.org/repo/xanjEo/images/859227167-Electra.png)

**Gradle configuration**
```
#!groovy

buildscript {
    repositories {
        mavenCentral()
    }
    dependencies {
        classpath 'com.android.tools.build:gradle:2.1.2'
        classpath 'com.neenbedankt.gradle.plugins:android-apt:1.8'
    }
}

apply plugin: 'com.android.application'
apply plugin: 'android-apt'

//....

compile 'org.bitbucket.txdrive:electra:1.0.0'
apt 'org.bitbucket.txdrive:electra-compiler:1.0.0'

```
## **[Documetation][1]** ##

# Discuss

To discuss or ask questions related to Electra, join to [Google+][2]
[1]: https://bitbucket.org/txdrive/electra/wiki/Home
[2]: https://plus.google.com/communities/111521734103502421003

License
=======

    Copyright 2016 Eugene Nadein

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.